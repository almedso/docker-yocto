docker run --rm --user $(id -u) \
  --volume=/var/volker-extended/yocto-ams:/yocto/root \
  --volume=/var/www/html:/yocto/publish \
  --volume=/var/volker-extended/download-cache:/yocto/download-cache \
  --env "CLEAN_BUILD=true" \
  --env "PUBLISH=true" \
  --env "INCLUDE_BUILD_HISTORY=true" \
  --memory=8Gb --cpus=0.0 \
  almedso/yocto-bitbaker $@