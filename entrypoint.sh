#!/usr/bin/env bash

help () {
    cat <<EOHELP
This container has the yocto prerequisites installed and supports
yocto 3.0 (zeuss) and newer.

the container checks

* if volumes are injected
* passes on control to a script **build.sh** located in root dir
  of the yocto project volume
* if no script is found - it starts an interactive bash shell
* script can be provided via -s or --script option + script path
* script path must be relative to the yocto root directory

Environment variables injected and provided inside the container
----------------------------------------------------------------

VOLUME_YOCTO_DIR:
    The directory where yocto sources are organized in
VOLUME_CACHE_DIR:
    The directory where yocto should place the download cache and sstate cache
VOLUME_PUBLISH_DIR:
    The directory build artifacts shall be published to
    is not defined if the volume is not injected

EOHELP
}

[ "$DEBUG" == 'true' ] && set -x  # aka set -o xtrace
set -o pipefail
set -o errexit  # aka set -e

CURRENT_DIR=$(pwd)

cleanup_and_exit () {
    echo "Cleaning stuff up..."
    cd $CURRENT_DIR
    set +x

    # good habit: clean up vars even if not neccessary
    unset CURRENT_DIR
    unset VOLUME_YOCTO_DIR
    unset VOLUME_CACHE_DIR
    unset VOLUME_PUBLISH_DIR
    unset BUILD_SCRIPT

    exit ${1:-0}
}

initialize_vars () {
    # CURRENTDIR = WORKDIR: this is either /yocto/root or overwritten via --workdir
    # in both cases it shall be a mounted volume
    export VOLUME_YOCTO_DIR=${CURRENT_DIR}
    # Allow that download volume or publish volume are injected to
    # specific directories
    # if so, it needs to be overwritten - exect for yocto root with is workdir
    export VOLUME_CACHE_DIR=${VOLUME_CACHE_DIR:-"/yocto/cache"}
    VOLUME_PUBLISH_DIR=${VOLUME_PUBLISH_DIR:-"/yocto/publish"}
    BUILD_SCRIPT=${VOLUME_YOCTO_DIR}/build.sh
}

sanity_checks () {
    # check if volumes are injected properly
    if ! grep -q ${VOLUME_CACHE_DIR} /proc/mounts ; then
        echo "ERROR: No cache volume (for download and shared state) provided"
        cleanup_and_exit 1
    fi
    local this_dir_volume=$(df .)
    echo "VOLUME with home: $this_dir_volume"
    if [[ $this_dir_volume =~ "overlay" || $this_dir_volume =~ "none" ]] ; then

        echo "ERROR: Working directory must be inside yocto root volume"
        echo "ERROR: Or alternative: No yocto root volume provided"
        cleanup_and_exit 1
    fi
    if ! grep -q ${VOLUME_PUBLISH_DIR} /proc/mounts ; then
        echo "WARNING: No publish volume provided"
        unset VOLUME_PUBLISH_DIR
    else
        export VOLUME_PUBLISH_DIR
    fi
}

process () {
    if [ -f "${BUILD_SCRIPT}" ]; then
        # runs the build script if available
        echo "INFO: Run build script"
        exec ${BUILD_SCRIPT}
    else
        if [ $# = 0 ]; then
            echo "INFO: Work interactive?  with --interactive --tty (-it)"
            /bin/bash
        else
            echo "INFO: Execute $@"
            exec "$@"
        fi
    fi
}

main () {
    trap cleanup_and_exit INT TERM
    initialize_vars
    case ${1} in
    -s|--script)
        BUILD_SCRIPT=${VOLUME_YOCTO_DIR}/$2
        echo "INFO: Use build script $2"
        if [ ! -f "${BUILD_SCRIPT}" ]; then
            echo "ERROR: Build script not found"
            cleanup_and_exit 1
        fi
        ;;
    --help|-?|help)
        help
        cleanup_and_exit
        ;;
    esac
    sanity_checks
    process "$@"
    cleanup_and_exit
}

main "$@"
