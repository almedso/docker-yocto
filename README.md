# Container for Yocto

## Features

* Enabled for yocto out of the box (based on ubuntu 20.04);
** Yocto builds are sanitzed on *ubuntu 20.04* starting with *zeus* release
* it does not enforce a yocto build user different from root
** either privatize your image (see below)
** or run a container with a different user (`--user $(id -u):$(id -g)`)
* git/ google repo
* rsync for copying

## How to create the docker image

Lets assume the image name shall be **yocto-bitbaker**.

Run in this directory (where this README.md file is located):

``` sh
docker build -t almedso/yocto-bitbaker .
```

### Publish the image

Run in this directory (where this README.md file is located):

``` sh
# docker tag <source> <target>
docker tag <image-id> almedso/yocto-bitbaker:20.04-02 # should correlate with ubuntu version
docker tag <image-id> almedso/yocto-bitbaker:latest
docker push almedso/yocto-bitbaker:latest
```

Note: You need to have a docker hub account and you have to login before.

Make sure the image is called **yocto-bitbaker** in order to make the code below work

# How to use the docker image

The user id of your user should be identical to the userid inside the container
in order to access the build output.

* the images defines the following volumes:
  * **/yocto/root** - this is the home of all Yocto activities
  * **/yocto/cache** - this is the home of all cached downloads and shared states
  * **/yocto/publish** - this is where images and packages are copied to for
    publishing (i.e. a repo server can point to) (if called with docker-compose
    it will be published right away at port 8080)
* Working directory is **/yocto/root**
* The user *yocto* is created and should be used is injected
  (i.e. yocto must not run as root)

## Get help of about container usage

to get help run

``` sh
docker run yocto-bitbaker help
# or
docker run yocto-bitbaker --help
# or
docker run yocto-bitbaker -?
```

For simplicity set as you want upfront:

``` sh
export YOCTO_ROOT="~/my-yocto/workdir"
export YOCTO_DONWLOAD="/my-yocto/download"
export YOCTO_PUBLISH="/my-html-base"
```

It is expected that a **build.sh** script exists in **${YOCTO_ROOT}** folder

``` sh
$ docker run --rm --user $(id -u):$(id -g) \
    --volume $YOCTO_ROOT:/yocto/root \
    --volume $YOCTO_CACHE:/yocto/cache \
    --volume $YOCTO_PUBLISH:/yocto/publish \
    yocto-bitbaker
```

alternatively, you can inject a script-path relative to your $YOCTO_ROOT.
e.g.

``` sh
$ docker run --rm --user $(id -u):$(id -g) \
    --volume $YOCTO_ROOT:/yocto/root \
    --volume $YOCTO_CACHE:/yocto/cache \
    --volume $YOCTO_PUBLISH:/yocto/publish \
    yocto-bitbaker --script scripts/my-build-script.sh
```

will execute:
*~/my-yocto/workdir/scripts/my-build-script.sh

## Run yocto builds interactively

You can also run Yocto builds interactively dockers interactive and
terminal options:

``` sh
$ docker run --rm --user $(id -u):$(id -g) \
    --volume $YOCTO_ROOT:/yocto/root \
    --volume $YOCTO_CACHE:/yocto/cache \
    --volume $YOCTO_PUBLISH:/yocto/publish \
    --interactive --tty yocto-bitbaker
```

The environment variables are set for interactive and script convenience:

* VOLUME_YOCTO_DIR
* VOLUME_CACHE_DIR
* VOLUME_PUBLISH_DIR

whereby the VOLUME_PUBLISH_DIR is not set if no volume is injected.
whereby the VOLUME_YOCTO_DIR is like Container workdir.

## Some more hints

You can also overwrite the workdir to your favorite yocto-root dir. and
provide a different another internal mount dir for Yocto root:

I.e. /yocto/root can be changed. like:

``` sh
$ WORKDIR=/new/work/dir
$ docker run --rm --memory 6G --user $(id -u) \
    --workdir $WORKDIR \
    --volume $YOCTO_ROOT:$WORKDIR \
    --volume $YOCTO_CACHE:/yocto/cache \
    --volume $YOCTO_PUBLISH:/yocto/publish \
    --interactive --tty yocto-bitbaker
```

A very simple script (without publishing) might look as follows:

``` sh
#!/usr/bin/env bash
source sources/poky/oe-init-build-env
build core-image-minimal
```

## Build and use a privatized Container

The folder *privatized-docker-image* contains a build script, instructions in
a README.md and the spec itself (Dockerfile) to create a privatized image.

The privatization just binds the image to your user and is called **my-**yocto-bitbaker
the general image is (prefixed) by my.

Make the *privatized-docker-image* folder available to your project, such that
you can create the privatized yocto builder image at any host where the
yocto build of your porject shall be done.
