# This container implements a yocto builder.
#
# Characteristics:
# * yocto standard toolchain
# * applicabale for yocto 3.0 (zeus) and newer

FROM ubuntu:20.04
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
  apt-utils \
  build-essential \
  cpio \
  chrpath \
  curl \
  diffstat \
  debianutils \
  gawk \
  gcc-multilib \
  git \
  g++-multilib \
  iputils-ping \
  libegl1-mesa \
  libsdl1.2-dev \
  lz4 \
  python3 \
  python3-git \
  python3-jinja2 \
  python3-pexpect \
  python3-pip \
  qemu \
  rsync \
  socat \
  texinfo \
  tig \
  tmux \
  unzip \
  vim \
  wget \
  xz-utils \
  xterm \
  zstd \
  && apt-get remove oss4-dev \
  && apt-get autoremove && apt-get clean
# package list according to
# https://www.yoctoproject.org/docs/current/ref-manual/ref-manual.html#ubuntu-packages
# plus apt-utils - is needed for proper configuration of packages
# plus repo tool - so this container can be used for repo operations
# plus rsync - to allow package synchronization with publish folder
# minus python2 - not needed in this list anymore; checked by building some images
# instead of qemu be installed - buld-dep is proposed by yocto manual
#  i.e. apt-get build-dep qemu
# plus tmux for better terminal support
# plus git and tig

# install repo tool
# repo tool requires the link to python (python2 is not installed purposefully)
# The yocto/root directory must exist in order to set the work dir to it
RUN mkdir -p /usr/local/bin && \
    curl https://storage.googleapis.com/git-repo-downloads/repo > /usr/local/bin/repo && \
    chmod a+rx /usr/local/bin/repo && \
	mkdir -p /yocto/root
# Fix the python call for python 3# Fix the python call for python 3
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 10 && \
    update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 10

# set the locale setting appropriately otherwise bitbake complains
ENV LANG=en_US.UTF-8
ENV TZ=Europe/Berlin
RUN DEBIAN_FRONTEND=noninteractive apt-get install locales -y \
  && locale-gen "en_US.UTF-8" && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
# Install the ronto tool as root.
RUN pip3 install ronto

COPY entrypoint.sh /usr/bin/
WORKDIR /yocto/root
ENTRYPOINT [ "/usr/bin/entrypoint.sh" ]

